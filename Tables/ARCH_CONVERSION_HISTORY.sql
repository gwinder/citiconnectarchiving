  DROP TABLE "ARCH_CONVERSION_HISTORY";
  CREATE TABLE "ARCH_CONVERSION_HISTORY" 
   (	"CONVERSION_ID" NUMBER NOT NULL ENABLE, 
	"CUSTOMER_ID" NUMBER NOT NULL ENABLE, 
	"INSTANCE_ID" NUMBER, 
	"EVENT_QUEUE_ID" NUMBER, 
	"LINK_ID" NUMBER, 
	"TOTAL_VALUE" NUMBER, 
	"INVOICE_NUMBER" VARCHAR2(80 BYTE), 
	"EMAIL_ADDRESS" VARCHAR2(256 BYTE), 
	"REFERRAL_IND" CHAR(1 BYTE), 
	"UPDATE_DATETIME" DATE, 
	"SEQUENCE_ID" NUMBER, 
	"USER_AGENT_ID" NUMBER, 
	 CONSTRAINT "PK_CONVERSION_HISTORY" PRIMARY KEY ("CONVERSION_ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS NOCOMPRESS LOGGING
  STORAGE( INITIAL 65536 NEXT 1048576 MINEXTENTS 1
  FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "ARCHIVE_DATA"  ENABLE
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE( INITIAL 65536 NEXT 1048576 MINEXTENTS 1
  FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "ARCHIVE_DATA" ;

  CREATE INDEX "CONVERSION_HIS_SEQ_ID" ON "ARCH_CONVERSION_HISTORY" ("SEQUENCE_ID", "CUSTOMER_ID", "UPDATE_DATETIME", "REFERRAL_IND") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS NOCOMPRESS LOGGING
  STORAGE( INITIAL 65536 NEXT 1048576 MINEXTENTS 1
  FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "ARCHIVE_DATA" ; 

  CREATE UNIQUE INDEX "CONVHIST_U_INST_INVOICE" ON "ARCH_CONVERSION_HISTORY" ("INSTANCE_ID", "INVOICE_NUMBER") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS NOCOMPRESS LOGGING
  STORAGE( INITIAL 65536 NEXT 1048576 MINEXTENTS 1
  FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "ARCHIVE_DATA" ;