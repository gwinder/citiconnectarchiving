 DROP TABLE ARCH_SMPP_MSG_ID;
 CREATE TABLE "ARCH_SMPP_MSG_ID" 
   (	"SMPP_MSGID" VARCHAR2(240 BYTE) NOT NULL ENABLE, 
	"CUSTOMER_ID" NUMBER NOT NULL ENABLE, 
	"INSTANCE_ID" NUMBER DEFAULT -1 NOT NULL ENABLE, 
	"EVENT_QUEUE_ID" NUMBER DEFAULT -1 NOT NULL ENABLE, 
	"SEQUENCE_NUMBER" NUMBER DEFAULT -1 NOT NULL ENABLE, 
	"CONTENT_START_POS" NUMBER DEFAULT -1 NOT NULL ENABLE, 
	"CONTENT_LENGTH" NUMBER DEFAULT -1 NOT NULL ENABLE, 
	"SENT_DATETIME" DATE
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "ARCHIVE_DATA" ;