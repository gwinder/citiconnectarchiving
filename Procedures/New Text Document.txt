create or replace procedure find_archivable_customers (age_param integer, comp_id number) as
  err_num NUMBER;
  err_msg VARCHAR2(100);
	age integer;
	cust_id number;
	cutoff_date date;
	cursor connect_archive_c (cutoff in date, comp_id in number)  is
		select customer_id, company_id from customer_master where company_id = comp_id and customer_id not in (SELECT customer_id FROM user_master) and update_datetime < cutoff ;
begin
	/* Clear list of customers*/
	delete from arch_customertodelete; 
  commit;
	age := nvl(age_param, 9999);
	select sysdate - age into cutoff_date from dual;
	/* Loop through all the customers older than the specified age */
	for connect_archive_rec in connect_archive_c(cutoff_date, comp_id)
  loop
  begin
  cust_id :=  connect_archive_rec.customer_id;
			insert into arch_customertodelete (customer_id, company_id, archive)
			values (cust_id, comp_id, 0); 
      commit;
  exception
  When others then 
  Rollback;
  err_num := SQLCODE;
  err_msg := SUBSTR(SQLERRM,1, 100);
  update arch_customertodelete set archive=2, SQLCODE=err_num, SQLERRM=err_msg where customer_id=cust_id;
  commit;
   end;
end loop; 
end;