create or replace procedure find_purgeable_customers (age_param integer, dept_id number, comp_id number, limit_in number) as
  err_num NUMBER;
  err_msg VARCHAR2(100);
	age integer;
	cust_id number;
	batch_limit number;  
	cutoff_date date;
  	cursor connect_purge_c (cutoff in date, dept_id number, comp_id in number)  is
		SELECT distinct(ARCH_CUSTOMER_MASTER.CUSTOMER_ID), CONVERSATION_MASTER.DEPARTMENT_ID, ARCH_CUSTOMER_MASTER.COMPANY_ID FROM ARCH_CUSTOMER_MASTER INNER JOIN ARCH_CUST_SUBSCRIPTION ON ARCH_CUSTOMER_MASTER.CUSTOMER_ID = ARCH_CUST_SUBSCRIPTION.CUSTOMER_ID INNER JOIN CONVERSATION_MASTER ON CONVERSATION_MASTER.CONVERSATION_ID  = ARCH_CUST_SUBSCRIPTION.CONVERSATION_ID WHERE CONVERSATION_MASTER.DEPARTMENT_ID = dept_id and ARCH_CUSTOMER_MASTER.COMPANY_ID = comp_id and ARCH_CUSTOMER_MASTER.update_datetime < cutoff order by ARCH_CUSTOMER_MASTER.CUSTOMER_ID asc; 
    TYPE customer_ids_aat IS TABLE OF connect_purge_c%ROWTYPE INDEX BY PLS_INTEGER;
    l_customer_ids customer_ids_aat;
begin
	/* Clear list of customers*/
	delete from arch_customertopurge;
  commit;
  batch_limit := nvl(limit_in, 10);
	age := nvl(age_param, 9999);
	select sysdate - age into cutoff_date from dual;
    open connect_purge_c(cutoff_date, dept_id, comp_id);
    LOOP
    -- bulk select list of customer ids
 		Fetch connect_purge_c
    BULK COLLECT INTO l_customer_ids LIMIT batch_limit;
    --
    FOR indx IN 1 .. l_customer_ids.COUNT 
    LOOP
        insert into arch_customertopurge(customer_id, company_id, department_id, purge) values (l_customer_ids(indx).customer_id, l_customer_ids(indx).company_id, l_customer_ids(indx).department_id, 0);
        END LOOP;
        EXIT WHEN l_customer_ids.COUNT < batch_limit;
      --
      -- NOW COMMIT
      --
      COMMIT;
    END LOOP;
  close connect_purge_c;
end;
/