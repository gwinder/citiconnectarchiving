create or replace procedure remove_archived_customers(comp_id number, dept_id number) as
  err_num NUMBER;
  err_msg VARCHAR2(100);
	cust_id integer;
  out_File UTL_FILE.FILE_TYPE;
  PURGE_LOG varchar(255);
	msgtype integer;
	opentran number(1);
/* Brickst */
/* Start of Standard Code */
/* Brickst */
TYPE idarray IS varray(5000) OF NUMBER; /* XXX HARDCODE FETCHSIZE */
  ids idarray;
  tabexists NUMBER;
  fetchsize NUMBER;
  transize NUMBER;
/* Brickst */
/* End of Standard Code */
/* Brickst */
begin
PURGE_LOG := 'C:\Tomcat-32\webapps\Citi';
/* Brickst */
/* Start of Standard Code */
/* Brickst */
 transize := 100;
 fetchsize := transize * 10;  /* CHANGE THIS THEN CHANGE LOOP COUNTER BELOW */
/* Brickst */
/* End of Standard Code */
/* Brickst */
/* Open Logfile */
Dbms_Output.put_line('Opening Logfile');
out_File := Utl_File.FOpen('ARCH_LOG', 'Remove_Connect_Customer_log.txt', 'W');
Dbms_Output.put_line('Opened Logfile');
/* Brickst */
/* Start of Standard Code only driver table and log table name changes*/
/* Brickst */
    LOOP
    -- bulk select list of customer ids
    -- note: all exists_* fields should be 'N'
    -- operator can manually override checks by setting a field to 'N'
    SELECT customer_id
    BULK COLLECT INTO ids
    from arch_customertodelete where archive=1 and company_id=comp_id and department_id=dept_id
    AND ROWNUM <= fetchsize;
    Dbms_Output.put_line('outer loop: fetched ' || ids.Count);
    EXIT WHEN ids.Count = 0;
    Dbms_Output.put_line('No Exit ' || ids.Count);
    --
    -- delete the customer records we just selected
    -- commit transactions in batches of transize
    FOR tnum IN 0..9 LOOP       /* ASSUMES FETCH SIZE IS TRANSIZE*10 */
      EXIT WHEN ids.Count <= tnum*transize;
 --     Dbms_Output.put_line('delete pass start ' || tnum);
      --
      -- NB: WE DO NOT CLEAN UP CUSTOMER_QUEUE, EVENT_QUEUE, and USER_MASTER
      -- ALWAYS RUN THE CHECK PROC TO LOOK FOR LIVE LINKS FROM THESE TABLES!!!
      --
      --
      -- clean up events
      --
      FORALL idx IN indices OF ids BETWEEN (1+tnum*transize) AND ((tnum+1)*transize)
        DELETE FROM event_queue_xml
        WHERE event_queue_id
        IN (SELECT event_queue_id FROM event_queue WHERE customer_id = ids(idx));
      FORALL idx IN indices OF ids BETWEEN (1+tnum*transize) AND ((tnum+1)*transize)
        DELETE FROM event_queue_detail
        WHERE event_queue_id
        IN (SELECT event_queue_id FROM event_queue WHERE customer_id = ids(idx));
       FORALL idx IN indices OF ids BETWEEN (1+tnum*transize) AND ((tnum+1)*transize)
         DELETE FROM event_queue WHERE customer_id = ids(idx);
      --
      -- clean up  SMPP history
      --
      FORALL idx IN indices OF ids BETWEEN (1+tnum*transize) AND ((tnum+1)*transize)
        DELETE FROM smpp_msg_id WHERE customer_id = ids(idx);
              --
      -- clean up message history
      --
     FORALL idx IN indices OF ids BETWEEN (1+tnum*transize) AND ((tnum+1)*transize)
        DELETE FROM message_history_content
        WHERE message_history_id
        IN (SELECT message_history_id FROM message_history WHERE customer_id = ids(idx));
      FORALL idx IN indices OF ids BETWEEN (1+tnum*transize) AND ((tnum+1)*transize)
        DELETE FROM message_history WHERE customer_id = ids(idx);
      --
      -- behavior history records
      --
      FORALL idx IN indices OF ids BETWEEN (1+tnum*transize) AND ((tnum+1)*transize)
        DELETE FROM click_history WHERE customer_id = ids(idx);
      FORALL idx IN indices OF ids BETWEEN (1+tnum*transize) AND ((tnum+1)*transize)
        DELETE FROM reply_history WHERE customer_id = ids(idx);
      FORALL idx IN indices OF ids BETWEEN (1+tnum*transize) AND ((tnum+1)*transize)
        DELETE FROM unsubscription_history WHERE customer_id = ids(idx);
      FORALL idx IN indices OF ids BETWEEN (1+tnum*transize) AND ((tnum+1)*transize)
        DELETE FROM conversion_history WHERE customer_id = ids(idx);
      -- NOTE: referral_history has two customer id fields
      FORALL idx IN indices OF ids BETWEEN (1+tnum*transize) AND ((tnum+1)*transize)
        DELETE FROM referral_history WHERE customer_id = ids(idx) OR referred_customer_id = ids(idx);
      --
      -- delete stats records
      --
      FORALL idx IN indices OF ids BETWEEN (1+tnum*transize) AND ((tnum+1)*transize)
        DELETE FROM cust_month_stats WHERE customer_id = ids(idx);
      FORALL idx IN indices OF ids BETWEEN (1+tnum*transize) AND ((tnum+1)*transize)
        DELETE FROM cust_week_stats WHERE customer_id = ids(idx);
     FORALL idx IN indices OF ids BETWEEN (1+tnum*transize) AND ((tnum+1)*transize)
        DELETE FROM cust_recent_stats WHERE customer_id = ids(idx);
      FORALL idx IN indices OF ids BETWEEN (1+tnum*transize) AND ((tnum+1)*transize)
        DELETE FROM cust_interest_stats WHERE customer_id = ids(idx);
      --
      -- delete subscriptions
      --
      FORALL idx IN indices OF ids BETWEEN (1+tnum*transize) AND ((tnum+1)*transize)
        DELETE FROM cust_subscription WHERE customer_id = ids(idx);
      --
      -- delete core customer data
      --
      FORALL idx IN indices OF ids BETWEEN (1+tnum*transize) AND ((tnum+1)*transize)
      DELETE FROM cust_attributes WHERE customer_id = ids(idx);
      FORALL idx IN indices OF ids BETWEEN (1+tnum*transize) AND ((tnum+1)*transize)
        DELETE FROM cust_attributes_history WHERE customer_id = ids(idx);
      FORALL idx IN indices OF ids BETWEEN (1+tnum*transize) AND ((tnum+1)*transize)
        DELETE FROM cust_preferences WHERE customer_id = ids(idx);
      FORALL idx IN indices OF ids BETWEEN (1+tnum*transize) AND ((tnum+1)*transize)
        DELETE FROM cust_email WHERE customer_id = ids(idx);
      FORALL idx IN indices OF ids BETWEEN (1+tnum*transize) AND ((tnum+1)*transize)
        DELETE FROM customer_equivalence WHERE customer_id = ids(idx);
     FORALL idx IN indices OF ids BETWEEN (1+tnum*transize) AND ((tnum+1)*transize)
        DELETE FROM customer_master WHERE customer_id = ids(idx);
      --
      -- NOW MUST UPDATE OUR RECORDS FROM CUST_PURGE_CANDIDATES TO AVOID INFINITE LOOP
      --
      FORALL idx IN indices OF ids BETWEEN (1+tnum*transize) AND ((tnum+1)*transize)
        update arch_customertodelete set archive=2 where customer_id = ids(idx);
      COMMIT;
                Dbms_Output.put_line('delete pass end ' || tnum);
    END LOOP;
  END LOOP;
/* Brickst */
/* End of Standard Code */
/* Brickst */
  exception
  When others then
  Rollback;
  err_num := SQLCODE;
  err_msg := SUBSTR(SQLERRM,1, 100);
  update arch_customertodelete set archive=2, SQLCODE=err_num, SQLERRM=err_msg where customer_id=cust_id;
  commit;
/* Tidy Up */
Utl_File.FClose(out_File);
/* Commit the Procedure */
  commit;
end;
/