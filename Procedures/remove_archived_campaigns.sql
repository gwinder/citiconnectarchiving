create or replace procedure remove_archived_campaigns(comp_id number, dept_id number) as
  err_num NUMBER;
  err_msg VARCHAR2(100);
	camp_id integer;
  out_File UTL_FILE.FILE_TYPE;
  PURGE_LOG varchar(255);
	msgtype integer;
	opentran number(1);
/* Brickst */
/* Start of Standard Code */
/* Brickst */
TYPE idarray IS varray(5000) OF NUMBER; /* XXX HARDCODE FETCHSIZE */
  ids idarray;
  tabexists NUMBER;
  fetchsize NUMBER;
  transize NUMBER;
/* Brickst */
/* End of Standard Code */
/* Brickst */
begin
PURGE_LOG := 'C:\Tomcat-32\webapps\Citi';
/* Brickst */
/* Start of Standard Code */
/* Brickst */
 transize := 1;
 fetchsize := transize * 10;  /* CHANGE THIS THEN CHANGE LOOP COUNTER BELOW */
/* Brickst */
/* End of Standard Code */
/* Brickst */
/* Open Logfile */
out_File := Utl_File.FOpen('PURGE_LOG', 'Remove_Connect_Campaign_log.txt', 'W');
/* Brickst */
/* Start of Standard Code only driver table and log table name changes*/
/* Brickst */
    UTL_FILE.PUT_LINE(out_File, 'Starting Delete');
    LOOP
    -- bulk select list of campaign ids
    -- note: all exists_* fields should be 'N'
    -- operator can manually override checks by setting a field to 'N'
    SELECT campaign_id
    BULK COLLECT INTO ids
    from arch_campaigntodelete where archive=1 and company_id=comp_id and department_id=dept_id
    AND ROWNUM <= fetchsize order by Campaign_ID asc;

    EXIT WHEN ids.Count = 0;
    UTL_FILE.PUT_LINE(out_File, 'Deleting Batch # = ' || ids.Count);
    --
    -- delete the campaign records we just selected
    -- commit transactions in batches of transize
    FOR tnum IN 0..9 LOOP       /* ASSUMES FETCH SIZE IS TRANSIZE*10 */
      EXIT WHEN ids.Count <= tnum*transize;
      --
      -- delete core campaign data
      --     --
      -- Audience Counts
      --
      begin
      FORALL idx IN indices OF ids BETWEEN (1+tnum*transize) AND ((tnum+1)*transize)
        DELETE FROM Audience_Counts WHERE campaign_id = ids(idx);
        Exception when others then
        UTL_FILE.PUT_LINE(out_File, 'Audience_Counts-' || SQLCODE ||'-'|| SQLERRM); 
      end;
      --      UTL_FILE.PUT_LINE(out_File, 'Breakpoint 1');
      --
      -- Message Attachments
      --
      begin
      FORALL idx IN indices OF ids BETWEEN (1+tnum*transize) AND ((tnum+1)*transize)
        DELETE FROM Message_Attachments WHERE message_id in (select message_id from message_master where campaign_id = ids(idx));
        Exception when others then
        UTL_FILE.PUT_LINE(out_File, 'Message_Attachments-' || SQLCODE ||'-'|| SQLERRM); 
      end;  
      --              UTL_FILE.PUT_LINE(out_File, 'Breakpoint 2');
      --
      -- Interaction Approvals
      --
      begin
      FORALL idx IN indices OF ids BETWEEN (1+tnum*transize) AND ((tnum+1)*transize)
        DELETE FROM Interaction_approvals WHERE interaction_id in (select interaction_id from interaction_master where campaign_id = ids(idx));
        Exception when others then
        UTL_FILE.PUT_LINE(out_File, 'Interaction_approvals-' || SQLCODE ||'-'|| SQLERRM); 
      end;
      --              UTL_FILE.PUT_LINE(out_File, 'Breakpoint 3');
      --
      -- Interaction Group Details
      --
      begin
      FORALL idx IN indices OF ids BETWEEN (1+tnum*transize) AND ((tnum+1)*transize)
        DELETE FROM Interaction_Group_Detail WHERE interaction_id in (select interaction_id from interaction_master where campaign_id = ids(idx));
        Exception when others then
        UTL_FILE.PUT_LINE(out_File, 'Interaction_Group_Detail-' || SQLCODE ||'-'|| SQLERRM); 
      end;
      --      UTL_FILE.PUT_LINE(out_File, 'Breakpoint 4');
      --
      -- Message Bodies
      --
      begin
      FORALL idx IN indices OF ids BETWEEN (1+tnum*transize) AND ((tnum+1)*transize)
        DELETE FROM Message_Bodies WHERE message_id in (select message_id from message_master where campaign_id = ids(idx));
        Exception when others then
        UTL_FILE.PUT_LINE(out_File, 'Message_Bodies-' || SQLCODE ||'-'|| SQLERRM); 
      end;        
      --              UTL_FILE.PUT_LINE(out_File, 'Breakpoint 5');
      --
      -- Message Parameters
      --
      begin
      FORALL idx IN indices OF ids BETWEEN (1+tnum*transize) AND ((tnum+1)*transize)
        DELETE FROM Message_Parameters WHERE message_id in (select message_id from message_master where campaign_id = ids(idx));
        Exception when others then
        UTL_FILE.PUT_LINE(out_File, 'Message_Parameters-' || SQLCODE ||'-'|| SQLERRM); 
      end;         
       --             UTL_FILE.PUT_LINE(out_File, 'Breakpoint 6');
       --
      -- Message Items
      --
      begin
      FORALL idx IN indices OF ids BETWEEN (1+tnum*transize) AND ((tnum+1)*transize)
        DELETE FROM Message_Items WHERE message_id in (select message_id from message_master where campaign_id = ids(idx));
        Exception when others then
        UTL_FILE.PUT_LINE(out_File, 'Message_Items-' || SQLCODE ||'-'|| SQLERRM); 
      end;         
       --             UTL_FILE.PUT_LINE(out_File, 'Breakpoint 7');
       --
      -- clean up SMPP MSG ID
      --
      begin
      FORALL idx IN indices OF ids BETWEEN (1+tnum*transize) AND ((tnum+1)*transize)
        DELETE FROM SMPP_MSG_ID Where INSTANCE_ID in (SELECT INSTANCE_QUEUE.INSTANCE_ID FROM INTERACTION_MASTER INNER JOIN INSTANCE_QUEUE ON INSTANCE_QUEUE.INTERACTION_ID = INTERACTION_MASTER.INTERACTION_ID WHERE INTERACTION_MASTER.CAMPAIGN_ID = ids(idx));
        Exception when others then
        UTL_FILE.PUT_LINE(out_File, 'SMPP_MSG_ID-' || SQLCODE ||'-'|| SQLERRM); 
      end; 
     --             UTL_FILE.PUT_LINE(out_File, 'Breakpoint 8');
      --
      -- Campaign Demo Group
      --
      begin
      FORALL idx IN indices OF ids BETWEEN (1+tnum*transize) AND ((tnum+1)*transize)
        DELETE FROM Campaign_Demo_Group WHERE campaign_id = ids(idx);
        Exception when others then
        UTL_FILE.PUT_LINE(out_File, 'Campaign_Demo_Groups-' || SQLCODE ||'-'|| SQLERRM); 
      end;        
      --              UTL_FILE.PUT_LINE(out_File, 'Breakpoint 9');
      --
      -- Campaign Group Detail
      --
      begin
      FORALL idx IN indices OF ids BETWEEN (1+tnum*transize) AND ((tnum+1)*transize)
        DELETE FROM Campaign_Group_Detail WHERE campaign_id = ids(idx);
        Exception when others then
        UTL_FILE.PUT_LINE(out_File, 'Campaign_Group_Detail-' || SQLCODE ||'-'|| SQLERRM); 
      end;          
      --              UTL_FILE.PUT_LINE(out_File, 'Breakpoint 110');
     --
      -- Customer Queue
      --
      begin
      FORALL idx IN indices OF ids BETWEEN (1+tnum*transize) AND ((tnum+1)*transize)
        DELETE FROM customer_queue WHERE campaign_id = ids(idx);
        Exception when others then
        UTL_FILE.PUT_LINE(out_File, 'customer_queue-' || SQLCODE ||'-'|| SQLERRM); 
      end;  
      --              UTL_FILE.PUT_LINE(out_File, 'Breakpoint 11');
      --
      -- Error2 Event Queue Attachment
      --
      begin
      FORALL idx IN indices OF ids BETWEEN (1+tnum*transize) AND ((tnum+1)*transize)
        DELETE FROM error2_event_queue_attachment WHERE event_queue_id in (SELECT EVENT_INTERACTION_QUEUE.EVENT_QUEUE_ID FROM EVENT_INTERACTION_QUEUE INNER JOIN INTERACTION_MASTER ON EVENT_INTERACTION_QUEUE.INTERACTION_ID = INTERACTION_MASTER.INTERACTION_ID WHERE INTERACTION_MASTER.CAMPAIGN_ID= ids(idx));
        Exception when others then
        UTL_FILE.PUT_LINE(out_File, 'error2_event_queue_attachment-' || SQLCODE ||'-'|| SQLERRM); 
      end;
      --               UTL_FILE.PUT_LINE(out_File, 'Breakpoint 12');
     --
      -- Error2 Event Queue Detail
      --
      begin
      FORALL idx IN indices OF ids BETWEEN (1+tnum*transize) AND ((tnum+1)*transize)
        DELETE FROM error2_event_queue_detail WHERE event_queue_id in (SELECT EVENT_INTERACTION_QUEUE.EVENT_QUEUE_ID FROM EVENT_INTERACTION_QUEUE INNER JOIN INTERACTION_MASTER ON EVENT_INTERACTION_QUEUE.INTERACTION_ID = INTERACTION_MASTER.INTERACTION_ID WHERE INTERACTION_MASTER.CAMPAIGN_ID= ids(idx));
        Exception when others then
        UTL_FILE.PUT_LINE(out_File, 'error2_event_queue_detail-' || SQLCODE ||'-'|| SQLERRM); 
      end;
      --              UTL_FILE.PUT_LINE(out_File, 'Breakpoint 13');
      --
      -- Error2 Event Queue
      --
      begin
      FORALL idx IN indices OF ids BETWEEN (1+tnum*transize) AND ((tnum+1)*transize)
        DELETE FROM error2_event_queue WHERE event_queue_id in (SELECT EVENT_INTERACTION_QUEUE.EVENT_QUEUE_ID FROM EVENT_INTERACTION_QUEUE INNER JOIN INTERACTION_MASTER ON EVENT_INTERACTION_QUEUE.INTERACTION_ID = INTERACTION_MASTER.INTERACTION_ID WHERE INTERACTION_MASTER.CAMPAIGN_ID= ids(idx));
              Exception when others then
        UTL_FILE.PUT_LINE(out_File, 'error2_event_queue-' || SQLCODE ||'-'|| SQLERRM); 
      end;
      --              UTL_FILE.PUT_LINE(out_File, 'Breakpoint 14');
      --
      -- Error Event Queue Detail
      --
      begin
      FORALL idx IN indices OF ids BETWEEN (1+tnum*transize) AND ((tnum+1)*transize)
        DELETE FROM error_event_queue_detail WHERE event_queue_id in (SELECT EVENT_INTERACTION_QUEUE.EVENT_QUEUE_ID FROM EVENT_INTERACTION_QUEUE INNER JOIN INTERACTION_MASTER ON EVENT_INTERACTION_QUEUE.INTERACTION_ID = INTERACTION_MASTER.INTERACTION_ID WHERE INTERACTION_MASTER.CAMPAIGN_ID= ids(idx));
              Exception when others then
        UTL_FILE.PUT_LINE(out_File, 'error_event_queue_detail-' || SQLCODE ||'-'|| SQLERRM); 
      end;
      --              UTL_FILE.PUT_LINE(out_File, 'Breakpoint 15');
      --
      -- Error Event Queue
      --
      begin
      FORALL idx IN indices OF ids BETWEEN (1+tnum*transize) AND ((tnum+1)*transize)
       DELETE FROM error_event_queue WHERE event_queue_id in (SELECT EVENT_INTERACTION_QUEUE.EVENT_QUEUE_ID FROM EVENT_INTERACTION_QUEUE INNER JOIN INTERACTION_MASTER ON EVENT_INTERACTION_QUEUE.INTERACTION_ID = INTERACTION_MASTER.INTERACTION_ID WHERE INTERACTION_MASTER.CAMPAIGN_ID= ids(idx));
         Exception when others then
        UTL_FILE.PUT_LINE(out_File, 'error_event_queue-' || SQLCODE ||'-'|| SQLERRM); 
      end;
      --    --            UTL_FILE.PUT_LINE(out_File, 'Breakpoint 16');
       --
      -- Event Queue
      --
      begin
      FORALL idx IN indices OF ids BETWEEN (1+tnum*transize) AND ((tnum+1)*transize)
        DELETE FROM event_queue WHERE event_queue_id in (SELECT EVENT_INTERACTION_QUEUE.EVENT_QUEUE_ID FROM EVENT_INTERACTION_QUEUE INNER JOIN INTERACTION_MASTER ON EVENT_INTERACTION_QUEUE.INTERACTION_ID = INTERACTION_MASTER.INTERACTION_ID WHERE INTERACTION_MASTER.CAMPAIGN_ID= ids(idx));
        Exception when others then
        UTL_FILE.PUT_LINE(out_File, 'event_queue-' || SQLCODE ||'-'|| SQLERRM); 
      end; --             UTL_FILE.PUT_LINE(out_File, 'Breakpoint 17');
      --
      -- clean up  SMPP history
      --
      begin
      FORALL idx IN indices OF ids BETWEEN (1+tnum*transize) AND ((tnum+1)*transize)
        DELETE FROM smpp_msg_id WHERE instance_id in (SELECT INSTANCE_QUEUE.INSTANCE_ID FROM INSTANCE_QUEUE INNER JOIN INTERACTION_MASTER ON INSTANCE_QUEUE.INTERACTION_ID = INTERACTION_MASTER.INTERACTION_ID WHERE INTERACTION_MASTER.CAMPAIGN_ID = ids(idx));
        Exception when others then
        UTL_FILE.PUT_LINE(out_File, 'smpp_msg_id-' || SQLCODE ||'-'|| SQLERRM); 
      end;
      --              UTL_FILE.PUT_LINE(out_File, 'Breakpoint 18');
      --
      -- clean up message history content
      --
      begin
      FORALL idx IN indices OF ids BETWEEN (1+tnum*transize) AND ((tnum+1)*transize)
        DELETE FROM message_history_content
        WHERE message_history_id
        IN (SELECT message_history_id FROM message_history WHERE campaign_id = ids(idx));
         Exception when others then
        UTL_FILE.PUT_LINE(out_File, 'message_history_content-' || SQLCODE ||'-'|| SQLERRM); 
      end;--              UTL_FILE.PUT_LINE(out_File, 'Breakpoint 19');
      --
      -- clean up message history
      --
      begin
      FORALL idx IN indices OF ids BETWEEN (1+tnum*transize) AND ((tnum+1)*transize)
        DELETE FROM message_history WHERE campaign_id = ids(idx);
        Exception when others then
        UTL_FILE.PUT_LINE(out_File, 'message_history-' || SQLCODE ||'-'|| SQLERRM); 
      end;
      --              UTL_FILE.PUT_LINE(out_File, 'Breakpoint 20');
      --
      -- clean up reply history
      --
      begin
      FORALL idx IN indices OF ids BETWEEN (1+tnum*transize) AND ((tnum+1)*transize)
        DELETE FROM reply_history WHERE instance_id in (SELECT INSTANCE_QUEUE.INSTANCE_ID FROM INSTANCE_QUEUE INNER JOIN INTERACTION_MASTER ON INSTANCE_QUEUE.INTERACTION_ID = INTERACTION_MASTER.INTERACTION_ID WHERE INTERACTION_MASTER.CAMPAIGN_ID = ids(idx));
        Exception when others then
        UTL_FILE.PUT_LINE(out_File, 'reply_history-' || SQLCODE ||'-'|| SQLERRM); 
      end;--              UTL_FILE.PUT_LINE(out_File, 'Breakpoint 21');
      --
      -- clean up click history
      --
      begin
      FORALL idx IN indices OF ids BETWEEN (1+tnum*transize) AND ((tnum+1)*transize)
        DELETE FROM click_history WHERE instance_id in (SELECT INSTANCE_QUEUE.INSTANCE_ID FROM INSTANCE_QUEUE INNER JOIN INTERACTION_MASTER ON INSTANCE_QUEUE.INTERACTION_ID = INTERACTION_MASTER.INTERACTION_ID WHERE INTERACTION_MASTER.CAMPAIGN_ID = ids(idx));
        Exception when others then
        UTL_FILE.PUT_LINE(out_File, 'click_history-' || SQLCODE ||'-'|| SQLERRM); 
      end;--              UTL_FILE.PUT_LINE(out_File, 'Breakpoint 22');
      --
      -- clean up subscription history
      --
      begin
      FORALL idx IN indices OF ids BETWEEN (1+tnum*transize) AND ((tnum+1)*transize)
        DELETE FROM subscription_history WHERE instance_id in (SELECT INSTANCE_QUEUE.INSTANCE_ID FROM INSTANCE_QUEUE INNER JOIN INTERACTION_MASTER ON INSTANCE_QUEUE.INTERACTION_ID = INTERACTION_MASTER.INTERACTION_ID WHERE INTERACTION_MASTER.CAMPAIGN_ID = ids(idx));
        Exception when others then
        UTL_FILE.PUT_LINE(out_File, 'subscription_history-' || SQLCODE ||'-'|| SQLERRM); 
      end;--              UTL_FILE.PUT_LINE(out_File, 'Breakpoint 23');
      --
      -- clean up unsubscription history
      --
      begin
      FORALL idx IN indices OF ids BETWEEN (1+tnum*transize) AND ((tnum+1)*transize)
        DELETE FROM unsubscription_history WHERE instance_id in (SELECT INSTANCE_QUEUE.INSTANCE_ID FROM INSTANCE_QUEUE INNER JOIN INTERACTION_MASTER ON INSTANCE_QUEUE.INTERACTION_ID = INTERACTION_MASTER.INTERACTION_ID WHERE INTERACTION_MASTER.CAMPAIGN_ID = ids(idx));
        Exception when others then
        UTL_FILE.PUT_LINE(out_File, 'unsubscription_history-' || SQLCODE ||'-'|| SQLERRM); 
      end;--              UTL_FILE.PUT_LINE(out_File, 'Breakpoint 24');
      --
      -- clean up conversion history
      --
      begin
      FORALL idx IN indices OF ids BETWEEN (1+tnum*transize) AND ((tnum+1)*transize)
        DELETE FROM conversion_history WHERE instance_id in (SELECT INSTANCE_QUEUE.INSTANCE_ID FROM INSTANCE_QUEUE INNER JOIN INTERACTION_MASTER ON INSTANCE_QUEUE.INTERACTION_ID = INTERACTION_MASTER.INTERACTION_ID WHERE INTERACTION_MASTER.CAMPAIGN_ID = ids(idx));
        Exception when others then
        UTL_FILE.PUT_LINE(out_File, 'conversion_history-' || SQLCODE ||'-'|| SQLERRM); 
      end;--              UTL_FILE.PUT_LINE(out_File, 'Breakpoint 25');
      --
      -- clean up msg demo seg link demo agg
      --
      begin
      FORALL idx IN indices OF ids BETWEEN (1+tnum*transize) AND ((tnum+1)*transize)
        DELETE FROM msg_seg_link_demo_agg WHERE instance_id in (SELECT INSTANCE_QUEUE.INSTANCE_ID FROM INSTANCE_QUEUE INNER JOIN INTERACTION_MASTER ON INSTANCE_QUEUE.INTERACTION_ID = INTERACTION_MASTER.INTERACTION_ID WHERE INTERACTION_MASTER.CAMPAIGN_ID = ids(idx));
        Exception when others then
        UTL_FILE.PUT_LINE(out_File, 'msg_seg_link_demo_agg-' || SQLCODE ||'-'|| SQLERRM); 
      end;
      --              UTL_FILE.PUT_LINE(out_File, 'Breakpoint 26');
      --
      -- clean up msg demo seg agg
      --
      begin
      FORALL idx IN indices OF ids BETWEEN (1+tnum*transize) AND ((tnum+1)*transize)
        DELETE FROM msg_demo_seg_agg WHERE instance_id in (SELECT INSTANCE_QUEUE.INSTANCE_ID FROM INSTANCE_QUEUE INNER JOIN INTERACTION_MASTER ON INSTANCE_QUEUE.INTERACTION_ID = INTERACTION_MASTER.INTERACTION_ID WHERE INTERACTION_MASTER.CAMPAIGN_ID = ids(idx));
         Exception when others then
        UTL_FILE.PUT_LINE(out_File, 'msg_demo_seg_agg-' || SQLCODE ||'-'|| SQLERRM); 
      end;
      --              UTL_FILE.PUT_LINE(out_File, 'Breakpoint 27');
      --
      -- clean up msg seg link agg
      --
      begin
      FORALL idx IN indices OF ids BETWEEN (1+tnum*transize) AND ((tnum+1)*transize)
        DELETE FROM msg_seg_link_agg WHERE instance_id in (SELECT INSTANCE_QUEUE.INSTANCE_ID FROM INSTANCE_QUEUE INNER JOIN INTERACTION_MASTER ON INSTANCE_QUEUE.INTERACTION_ID = INTERACTION_MASTER.INTERACTION_ID WHERE INTERACTION_MASTER.CAMPAIGN_ID = ids(idx));
        Exception when others then
        UTL_FILE.PUT_LINE(out_File, 'msg_seg_link_agg-' || SQLCODE ||'-'|| SQLERRM); 
      end;
      --              UTL_FILE.PUT_LINE(out_File, 'Breakpoint 28');
      --
      -- clean up Link Master
      --
      begin
      FORALL idx IN indices OF ids BETWEEN (1+tnum*transize) AND ((tnum+1)*transize)
        DELETE FROM link_master WHERE conversion_id in (SELECT CONVERSION_HISTORY.CONVERSION_ID FROM LINK_MASTER, CONVERSION_HISTORY INNER JOIN INSTANCE_QUEUE ON INSTANCE_QUEUE.INSTANCE_ID = CONVERSION_HISTORY.CUSTOMER_ID INNER JOIN INTERACTION_MASTER ON INTERACTION_MASTER.INTERACTION_ID = INSTANCE_QUEUE.INTERACTION_ID WHERE INTERACTION_MASTER.CAMPAIGN_ID = ids(idx));
        Exception when others then
        UTL_FILE.PUT_LINE(out_File, 'link_master-' || SQLCODE ||'-'|| SQLERRM); 
      end; 
      --              UTL_FILE.PUT_LINE(out_File, 'Breakpoint 29');
      --
      -- Instance Queue
      --
      begin
      FORALL idx IN indices OF ids BETWEEN (1+tnum*transize) AND ((tnum+1)*transize)
        DELETE FROM Instance_Queue WHERE interaction_ID in (select interaction_id from interaction_master where campaign_id = ids(idx));
        Exception when others then
        UTL_FILE.PUT_LINE(out_File, 'Instance_Queue-' || SQLCODE ||'-'|| SQLERRM); 
      end; 
      --              UTL_FILE.PUT_LINE(out_File, 'Breakpoint 30');
       --
      -- Interaction_to_Replyhandler
      --
      begin
      FORALL idx IN indices OF ids BETWEEN (1+tnum*transize) AND ((tnum+1)*transize)
        DELETE FROM Interaction_to_Replyhandler WHERE interaction_ID in (select interaction_id from interaction_master where campaign_id = ids(idx));
        Exception when others then
        UTL_FILE.PUT_LINE(out_File, 'Interaction_to_Replyhandler-' || SQLCODE ||'-'|| SQLERRM); 
      end;
      --              UTL_FILE.PUT_LINE(out_File, 'Breakpoint 31');
      --
      -- Interaction Master
      --
      begin
      FORALL idx IN indices OF ids BETWEEN (1+tnum*transize) AND ((tnum+1)*transize)
        DELETE FROM Interaction_Master WHERE campaign_id = ids(idx);
        Exception when others then
        UTL_FILE.PUT_LINE(out_File, 'Interaction_Master-' || SQLCODE ||'-'|| SQLERRM); 
      end;
      --             UTL_FILE.PUT_LINE(out_File, 'Breakpoint 32');
       --
      -- Message Master
      --
      begin
      FORALL idx IN indices OF ids BETWEEN (1+tnum*transize) AND ((tnum+1)*transize)
        DELETE FROM Message_Master WHERE campaign_id = ids(idx);
              Exception when others then
        UTL_FILE.PUT_LINE(out_File, 'Message_Master-' || SQLCODE ||'-'|| SQLERRM); 
      end;
      --              UTL_FILE.PUT_LINE(out_File, 'Breakpoint 33');
      --
      --
      -- Quiet Period
      --
      begin
      FORALL idx IN indices OF ids BETWEEN (1+tnum*transize) AND ((tnum+1)*transize)
        DELETE FROM Quiet_Period WHERE campaign_id = ids(idx);
        Exception when others then
        UTL_FILE.PUT_LINE(out_File, 'Quiet_Period-' || SQLCODE ||'-'|| SQLERRM); 
      end;
      --              UTL_FILE.PUT_LINE(out_File, 'Breakpoint 34');
      --
      -- Delete Master Record
      --
      begin
      FORALL idx IN indices OF ids BETWEEN (1+tnum*transize) AND ((tnum+1)*transize)
        DELETE FROM campaign_master WHERE campaign_id = ids(idx);
        Exception when others then
        UTL_FILE.PUT_LINE(out_File, 'Campaign_Master-' || SQLCODE ||'-'|| SQLERRM); 
      end; 
      --              UTL_FILE.PUT_LINE(out_File, 'Breakpoint 35');
      --
      -- NOW MUST UPDATE OUR RECORDS TO AVOID INFINITE LOOP
      --

      FORALL idx IN indices OF ids BETWEEN (1+tnum*transize) AND ((tnum+1)*transize)
        update arch_campaigntodelete set archive=2 where campaign_id = ids(idx);
      COMMIT;
    END LOOP;
    UTL_FILE.PUT_LINE(out_File, 'Deleted Batch # = ' || ids.Count);
  END LOOP;
/* Brickst */
/* End of Standard Code */
/* Brickst */
  exception
  When others then
  Rollback;
  err_num := SQLCODE;
  err_msg := SUBSTR(SQLERRM,1, 100);
  update arch_campaigntodelete set archive=2, SQLCODE=err_num, SQLERRM=err_msg where campaign_id=camp_id;
  commit;
/* Tidy Up */
Utl_File.FClose(out_File);
/* Commit the Procedure */
  commit;
end;