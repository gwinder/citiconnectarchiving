create or replace procedure find_archivable_campaigns (age_param integer, comp_id number, dept_id number) as
  err_num NUMBER;
  err_msg VARCHAR2(100);
	age integer;
	camp_id number;
	cutoff_date date;
	cursor connect_archive_c (cutoff in date, comp_id in number, dept_id number) is
		select campaign_id from campaign_master where company_id = comp_id and department_id = dept_id  and update_datetime < cutoff and campaign_type_code != 3;
begin
	/* Clear list of campaigns*/
	delete from arch_campaigntodelete; 
	age := nvl(age_param, 9999);
	select sysdate - age into cutoff_date from dual;
	/* Loop through all the campaigns older than the specified age */
	for connect_archive_rec in connect_archive_c(cutoff_date, comp_id, dept_id)
  loop
  begin
  camp_id :=  connect_archive_rec.campaign_id;
  dbms_output.put_line(camp_id);
			insert into arch_campaigntodelete (campaign_id, company_id, department_id, archive)
			values (camp_id, comp_id, dept_id, 0);
			commit;  
  exception
  When others then 
  Rollback;
  err_num := SQLCODE;
  err_msg := SUBSTR(SQLERRM,1, 100);
  update arch_campaigntodelete set archive=2, SQLCODE=err_num, SQLERRM=err_msg where campaign_id=camp_id;
  commit;
   end;
end loop; 
end;
/