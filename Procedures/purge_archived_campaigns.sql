create or replace procedure purge_archived_campaigns(comp_id number, dept_id number) as
  err_num NUMBER;
  err_msg VARCHAR2(100);
	camp_id integer;
  out_File UTL_FILE.FILE_TYPE;
  PURGE_LOG varchar(255);
	msgtype integer;
	opentran number(1);
/* Brickst */
/* Start of Standard Code */
/* Brickst */
TYPE idarray IS varray(5000) OF NUMBER; /* XXX HARDCODE FETCHSIZE */
  ids idarray;
  tabexists NUMBER;
  fetchsize NUMBER;
  transize NUMBER;
/* Brickst */
/* End of Standard Code */
/* Brickst */
begin
PURGE_LOG := 'C:\Tomcat-32\webapps\Citi';
/* Brickst */
/* Start of Standard Code */
/* Brickst */
 transize := 1;
 fetchsize := transize * 10;  /* CHANGE THIS THEN CHANGE LOOP COUNTER BELOW */
/* Brickst */
/* End of Standard Code */
/* Brickst */
/* Open Logfile */
out_File := Utl_File.FOpen('PURGE_LOG', 'Purge_Connect_Campaign_log.txt', 'W');
/* Brickst */
/* Start of Standard Code only driver table and log table name changes*/
/* Brickst */
    UTL_FILE.PUT_LINE(out_File, 'Starting Delete');
    LOOP
    -- bulk select list of campaign ids
    -- note: all exists_* fields should be 'N'
    -- operator can manually override checks by setting a field to 'N'
    SELECT campaign_id
    BULK COLLECT INTO ids
    from arch_campaigntopurge where purge=0 and company_id=comp_id and department_id=dept_id
    AND ROWNUM <= fetchsize order by Campaign_ID asc;

    EXIT WHEN ids.Count = 0;
    UTL_FILE.PUT_LINE(out_File, 'Deleting Batch # = ' || ids.Count);
    --
    -- delete the campaign records we just selected
    -- commit transactions in batches of transize
    FOR tnum IN 0..9 LOOP       /* ASSUMES FETCH SIZE IS TRANSIZE*10 */
      EXIT WHEN ids.Count <= tnum*transize;
      --
      -- delete core campaign data
      --     --
      -- ARCH_MESSAGE_HISTORY_CONTENT
      --
      FORALL idx IN indices OF ids BETWEEN (1+tnum*transize) AND ((tnum+1)*transize)
        DELETE FROM ARCH_MESSAGE_HISTORY_CONTENT WHERE message_history_id in (SELECT ARCH_MESSAGE_HISTORY.MESSAGE_HISTORY_ID FROM ARCH_INTERACTION_MASTER INNER JOIN ARCH_INSTANCE_QUEUE ON ARCH_INTERACTION_MASTER.INTERACTION_ID = ARCH_INSTANCE_QUEUE.INTERACTION_ID INNER JOIN ARCH_MESSAGE_HISTORY ON ARCH_MESSAGE_HISTORY.INSTANCE_ID = ARCH_INSTANCE_QUEUE.INTERACTION_ID and ARCH_INTERACTION_MASTER.CAMPAIGN_ID = ids(idx));
            UTL_FILE.PUT_LINE(out_File, 'Breakpoint 1');
      --
      -- ARCH_MESSAGE_HISTORY
      --
      FORALL idx IN indices OF ids BETWEEN (1+tnum*transize) AND ((tnum+1)*transize)
        DELETE FROM ARCH_MESSAGE_HISTORY WHERE instance_id in (SELECT ARCH_INSTANCE_QUEUE.INSTANCE_ID FROM ARCH_INTERACTION_MASTER INNER JOIN ARCH_INSTANCE_QUEUE ON ARCH_INTERACTION_MASTER.INTERACTION_ID = ARCH_INSTANCE_QUEUE.INTERACTION_ID and ARCH_INTERACTION_MASTER.CAMPAIGN_ID = ids(idx));
            UTL_FILE.PUT_LINE(out_File, 'Breakpoint 2');
      --
      -- Customer Queue
      --
      FORALL idx IN indices OF ids BETWEEN (1+tnum*transize) AND ((tnum+1)*transize)
        DELETE FROM Arch_Customer_Queue WHERE CAMPAIGN_ID = ids(idx);
                    UTL_FILE.PUT_LINE(out_File, 'Breakpoint 3');
      --
      -- message history
      --
      FORALL idx IN indices OF ids BETWEEN (1+tnum*transize) AND ((tnum+1)*transize)
        DELETE FROM arch_message_history WHERE campaign_id = ids(idx);
                    UTL_FILE.PUT_LINE(out_File, 'Breakpoint 4');
      --
      -- clean up reply history
      --
      FORALL idx IN indices OF ids BETWEEN (1+tnum*transize) AND ((tnum+1)*transize)
        DELETE FROM arch_reply_history WHERE instance_id in (SELECT ARCH_INSTANCE_QUEUE.INSTANCE_ID FROM ARCH_INSTANCE_QUEUE INNER JOIN ARCH_INTERACTION_MASTER ON ARCH_INSTANCE_QUEUE.INTERACTION_ID = ARCH_INTERACTION_MASTER.INTERACTION_ID WHERE ARCH_INTERACTION_MASTER.CAMPAIGN_ID = ids(idx));
                    UTL_FILE.PUT_LINE(out_File, 'Breakpoint 5');
      --
      -- clean up click history
      --
      FORALL idx IN indices OF ids BETWEEN (1+tnum*transize) AND ((tnum+1)*transize)
        DELETE FROM arch_click_history WHERE instance_id in (SELECT ARCH_INSTANCE_QUEUE.INSTANCE_ID FROM ARCH_INSTANCE_QUEUE INNER JOIN ARCH_INTERACTION_MASTER ON ARCH_INSTANCE_QUEUE.INTERACTION_ID = ARCH_INTERACTION_MASTER.INTERACTION_ID WHERE ARCH_INTERACTION_MASTER.CAMPAIGN_ID = ids(idx));
                    UTL_FILE.PUT_LINE(out_File, 'Breakpoint 6');
      --
      -- Unsubscribe Device History
      --
      FORALL idx IN indices OF ids BETWEEN (1+tnum*transize) AND ((tnum+1)*transize)
        DELETE FROM arch_unsub_device_history WHERE instance_id in (SELECT ARCH_INSTANCE_QUEUE.INSTANCE_ID FROM ARCH_INSTANCE_QUEUE INNER JOIN ARCH_INTERACTION_MASTER ON ARCH_INSTANCE_QUEUE.INTERACTION_ID = ARCH_INTERACTION_MASTER.INTERACTION_ID WHERE ARCH_INTERACTION_MASTER.CAMPAIGN_ID = ids(idx));
            UTL_FILE.PUT_LINE(out_File, 'Breakpoint 7');
      --
      -- Unsubscription  History
      --
      FORALL idx IN indices OF ids BETWEEN (1+tnum*transize) AND ((tnum+1)*transize)
        DELETE FROM arch_unsubscription_history WHERE instance_id in (SELECT ARCH_INSTANCE_QUEUE.INSTANCE_ID FROM ARCH_INSTANCE_QUEUE INNER JOIN ARCH_INTERACTION_MASTER ON ARCH_INSTANCE_QUEUE.INTERACTION_ID = ARCH_INTERACTION_MASTER.INTERACTION_ID WHERE ARCH_INTERACTION_MASTER.CAMPAIGN_ID = ids(idx));
            UTL_FILE.PUT_LINE(out_File, 'Breakpoint 8');
      --
      -- Subscription  History
      --
      FORALL idx IN indices OF ids BETWEEN (1+tnum*transize) AND ((tnum+1)*transize)
        DELETE FROM arch_subscription_history WHERE instance_id in (SELECT ARCH_INSTANCE_QUEUE.INSTANCE_ID FROM ARCH_INSTANCE_QUEUE INNER JOIN ARCH_INTERACTION_MASTER ON ARCH_INSTANCE_QUEUE.INTERACTION_ID = ARCH_INTERACTION_MASTER.INTERACTION_ID WHERE ARCH_INTERACTION_MASTER.CAMPAIGN_ID = ids(idx));
            UTL_FILE.PUT_LINE(out_File, 'Breakpoint 9');
      --
      -- Conversion  History
      --
      FORALL idx IN indices OF ids BETWEEN (1+tnum*transize) AND ((tnum+1)*transize)
        DELETE FROM arch_conversion_history WHERE instance_id in (SELECT ARCH_INSTANCE_QUEUE.INSTANCE_ID FROM ARCH_INSTANCE_QUEUE INNER JOIN ARCH_INTERACTION_MASTER ON ARCH_INSTANCE_QUEUE.INTERACTION_ID = ARCH_INTERACTION_MASTER.INTERACTION_ID WHERE ARCH_INTERACTION_MASTER.CAMPAIGN_ID = ids(idx));
            UTL_FILE.PUT_LINE(out_File, 'Breakpoint 10');         
      --
      -- Referral  History
      --
      FORALL idx IN indices OF ids BETWEEN (1+tnum*transize) AND ((tnum+1)*transize)
        DELETE FROM arch_referral_history WHERE instance_id in (SELECT ARCH_INSTANCE_QUEUE.INSTANCE_ID FROM ARCH_INSTANCE_QUEUE INNER JOIN ARCH_INTERACTION_MASTER ON ARCH_INSTANCE_QUEUE.INTERACTION_ID = ARCH_INTERACTION_MASTER.INTERACTION_ID WHERE ARCH_INTERACTION_MASTER.CAMPAIGN_ID = ids(idx));
            UTL_FILE.PUT_LINE(out_File, 'Breakpoint 11');
      --
      -- Instance Queue
      --
      FORALL idx IN indices OF ids BETWEEN (1+tnum*transize) AND ((tnum+1)*transize)
        DELETE FROM arch_instance_queue WHERE interaction_id in (SELECT ARCH_INTERACTION_MASTER.INTERACTION_ID from ARCH_INTERACTION_MASTER  WHERE ARCH_INTERACTION_MASTER.CAMPAIGN_ID = ids(idx));
            UTL_FILE.PUT_LINE(out_File, 'Breakpoint 12');
     --
      -- Interaction Master
      --
      FORALL idx IN indices OF ids BETWEEN (1+tnum*transize) AND ((tnum+1)*transize)
        DELETE FROM arch_interaction_master WHERE campaign_id = ids(idx);
            UTL_FILE.PUT_LINE(out_File, 'Breakpoint 13');
      --
      -- Delete Master Record
      --
      FORALL idx IN indices OF ids BETWEEN (1+tnum*transize) AND ((tnum+1)*transize)
        DELETE FROM arch_campaign_master WHERE campaign_id = ids(idx);
                    UTL_FILE.PUT_LINE(out_File, 'Breakpoint 14');
      --
      -- NOW MUST UPDATE OUR RECORDS FROM camp_PURGE_CANDIDATES TO AVOID INFINITE LOOP
      --
      FORALL idx IN indices OF ids BETWEEN (1+tnum*transize) AND ((tnum+1)*transize)
        update arch_campaigntopurge set purge=2 where campaign_id = ids(idx);
      COMMIT;
    END LOOP;
    UTL_FILE.PUT_LINE(out_File, 'Deleted Batch # = ' || ids.Count);
  END LOOP;
/* Brickst */
/* End of Standard Code */
/* Brickst */
  exception
  When others then
  Rollback;
  err_num := SQLCODE;
  err_msg := SUBSTR(SQLERRM,1, 100);
  update arch_campaigntopurge set purge=2, SQLCODE=err_num, SQLERRM=err_msg where campaign_id=camp_id;
  commit;
/* Tidy Up */
Utl_File.FClose(out_File);
/* Commit the Procedure */
  commit;
end;