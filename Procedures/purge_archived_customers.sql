create or replace procedure purge_archived_customers(comp_id number, dept_id number) as
  err_num NUMBER;
  err_msg VARCHAR2(100);
	cust_id integer;
  out_File UTL_FILE.FILE_TYPE;
  PURGE_LOG varchar(255);
	msgtype integer;
	opentran number(1);
/* Brickst */
/* Start of Standard Code */
/* Brickst */
TYPE idarray IS varray(5000) OF NUMBER; /* XXX HARDCODE FETCHSIZE */
  ids idarray;
  tabexists NUMBER;
  fetchsize NUMBER;
  transize NUMBER;
/* Brickst */
/* End of Standard Code */
/* Brickst */
begin
PURGE_LOG := 'C:\Tomcat-32\webapps\Citi';
/* Brickst */
/* Start of Standard Code */
/* Brickst */
 transize := 500;
 fetchsize := transize * 10;  /* CHANGE THIS THEN CHANGE LOOP COUNTER BELOW */
/* Brickst */
/* End of Standard Code */
/* Brickst */
/* Open Logfile */
out_File := Utl_File.FOpen('PURGE_LOG', 'Purge_Connect_Customer_log.txt', 'W');
/* Brickst */
/* Start of Standard Code only driver table and log table name changes*/
/* Brickst */
    LOOP
    -- bulk select list of customer ids
    -- note: all exists_* fields should be 'N'
    -- operator can manually override checks by setting a field to 'N'
    SELECT customer_id
    BULK COLLECT INTO ids
    from arch_customertopurge where purge=0 and company_id=comp_id and department_id=dept_id
    AND ROWNUM <= fetchsize order by CUSTOMER_ID asc;
    EXIT WHEN ids.Count = 0;
    --
    -- delete the customer records we just selected
    -- commit transactions in batches of transize
    FOR tnum IN 0..9 LOOP       /* ASSUMES FETCH SIZE IS TRANSIZE*10 */
      EXIT WHEN ids.Count <= tnum*transize;
      --
      -- delete core customer data
      --
      FORALL idx IN indices OF ids BETWEEN (1+tnum*transize) AND ((tnum+1)*transize)
        DELETE FROM arch_cust_attributes WHERE customer_id = ids(idx);
      FORALL idx IN indices OF ids BETWEEN (1+tnum*transize) AND ((tnum+1)*transize)
        DELETE FROM arch_cust_preferences WHERE customer_id = ids(idx);
      FORALL idx IN indices OF ids BETWEEN (1+tnum*transize) AND ((tnum+1)*transize)
        DELETE FROM arch_cust_subscription WHERE customer_id = ids(idx);
      FORALL idx IN indices OF ids BETWEEN (1+tnum*transize) AND ((tnum+1)*transize)
        DELETE FROM arch_customer_master WHERE customer_id = ids(idx);
      --
      -- NOW MUST UPDATE OUR RECORDS FROM CUST_PURGE_CANDIDATES TO AVOID INFINITE LOOP
      --
      FORALL idx IN indices OF ids BETWEEN (1+tnum*transize) AND ((tnum+1)*transize)
        update arch_customertopurge set purge=1 where customer_id = ids(idx);
      COMMIT;
    END LOOP;
    UTL_FILE.PUT_LINE(out_File, 'Purged Batch # = ' || ids.Count);
  END LOOP;
/* Brickst */
/* End of Standard Code */
/* Brickst */
  exception
  When others then
  Rollback;
  err_num := SQLCODE;
  err_msg := SUBSTR(SQLERRM,1, 100);
  update arch_customertopurge set purge=2, SQLCODE=err_num, SQLERRM=err_msg where customer_id=cust_id;
  commit;
/* Tidy Up */
Utl_File.FClose(out_File);
/* Commit the Procedure */
  commit;
end;
/