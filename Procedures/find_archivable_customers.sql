create or replace procedure find_archivable_customers (age_param integer, comp_id number, dept_id number, limit_in number) as
  err_num NUMBER;
  err_msg VARCHAR2(100);
	age integer;
	cust_id number;
	batch_limit number;  
	cutoff_date date;
  	cursor connect_archive_c (cutoff in date, dept_id number, comp_id in number)  is
		SELECT distinct(CUSTOMER_MASTER.CUSTOMER_ID), CONVERSATION_MASTER.DEPARTMENT_ID, CUSTOMER_MASTER.COMPANY_ID FROM CUSTOMER_MASTER INNER JOIN CUST_SUBSCRIPTION ON CUSTOMER_MASTER.CUSTOMER_ID = CUST_SUBSCRIPTION.CUSTOMER_ID INNER JOIN CONVERSATION_MASTER ON CONVERSATION_MASTER.CONVERSATION_ID  = CUST_SUBSCRIPTION.CONVERSATION_ID WHERE CONVERSATION_MASTER.DEPARTMENT_ID = dept_id and CUSTOMER_MASTER.COMPANY_ID = comp_id and CUSTOMER_MASTER.customer_id not in (SELECT customer_id FROM user_master) and CUSTOMER_MASTER.update_datetime < cutoff order by CUSTOMER_MASTER.CUSTOMER_ID asc; 
    TYPE customer_ids_aat IS TABLE OF connect_archive_c%ROWTYPE INDEX BY PLS_INTEGER;
    l_customer_ids customer_ids_aat;
begin
	/* Clear list of customers*/
	delete from arch_customertodelete;
  commit;
  batch_limit := nvl(limit_in, 10);
	age := nvl(age_param, 9999);
	select sysdate - age into cutoff_date from dual;
    open connect_archive_c(cutoff_date, dept_id, comp_id);
    LOOP
    -- bulk select list of customer ids
 		Fetch connect_archive_c
    BULK COLLECT INTO l_customer_ids LIMIT batch_limit;
    --
    FOR indx IN 1 .. l_customer_ids.COUNT 
    LOOP
        insert into arch_customertodelete(customer_id, company_id, department_id, archive) values (l_customer_ids(indx).customer_id, l_customer_ids(indx).company_id, l_customer_ids(indx).department_id, 0);
        END LOOP;
        EXIT WHEN l_customer_ids.COUNT < batch_limit;
      --
      -- NOW COMMIT
      --
      COMMIT;
    END LOOP;
  close connect_archive_c;
end;
/